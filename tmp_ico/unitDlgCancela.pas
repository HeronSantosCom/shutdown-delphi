unit unitDlgCancela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, CoolTrayIcon, StdCtrls;

type
  TfrmDlgCancela = class(TForm)
    imgSkin: TImage;
    btnSim: TSpeedButton;
    btnNao: TSpeedButton;
    Label1: TLabel;
    procedure btnSimClick(Sender: TObject);
    procedure btnNaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDlgCancela: TfrmDlgCancela;

implementation

uses unitPrincipal;

{$R *.dfm}

{   Sempre no topo  }
procedure ForceForegroundWindow(hwnd: THandle);
  // (W) 2001 Daniel Rolf
  // http://www.finecode.de
  // rolf@finecode.de
var
  hlp: TfrmDlgCancela;
begin
  hlp := TfrmDlgCancela.Create(nil);
  try
    hlp.BorderStyle := bsNone;
    hlp.SetBounds(0, 0, 1, 1);
    hlp.FormStyle := fsStayOnTop;
    hlp.Show;
    mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
    mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    SetForegroundWindow(hwnd);
  finally
    hlp.Free;
  end;
end;

procedure TfrmDlgCancela.btnSimClick(Sender: TObject);
begin
  {   DESLIGAR   }
  If frmPrincipal.cbxDesligar.Checked = True Then Begin
    WinExec('shutdown -a',0);
    frmPrincipal.cbxDesligar.Checked := False;
    frmPrincipal.btnLogoff.Enabled := True;
    frmPrincipal.btnReiniciar.Enabled := True;
    frmPrincipal.mkeAjust.Enabled := True;
    frmPrincipal.TrayIcon.IconList := frmPrincipal.iclNormal;
    frmPrincipal.TrayIcon.CycleIcons := False;
    frmPrincipal.tmrAjust.Enabled := False;
    frmDlgCancela.Close;
    frmPrincipal.TrayIcon.ShowMainForm;
    frmPrincipal.TrayIcon.ShowBalloonHint('Desligamento Autom�tico Desativado!', 'Voc� acaba de cancelar o Desligamento Autom�tico.', bitWarning, 10);
  End;
  {   REINICIAR   }
  If frmPrincipal.cbxReiniciar.Checked = True Then Begin
    WinExec('shutdown -a',0);
    frmPrincipal.cbxReiniciar.Checked := False;
    frmPrincipal.btnLogoff.Enabled := True;
    frmPrincipal.btnDesligar.Enabled := True;
    frmPrincipal.mkeAjust.Enabled := True;
    frmPrincipal.TrayIcon.IconList := frmPrincipal.iclNormal;
    frmPrincipal.TrayIcon.CycleIcons := False;
    frmPrincipal.tmrAjust.Enabled := False;
    frmDlgCancela.Close;
    frmPrincipal.TrayIcon.ShowMainForm;
    frmPrincipal.TrayIcon.ShowBalloonHint('Reiniciamento Autom�tico Desativado!', 'Voc� acaba de cancelar o Reiniciamento Autom�tico.', bitWarning, 10);
  End;
end;

procedure TfrmDlgCancela.btnNaoClick(Sender: TObject);
begin
  Halt;
end;

end.
