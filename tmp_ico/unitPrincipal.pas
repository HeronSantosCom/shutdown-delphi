unit unitPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, CoolTrayIcon, ImgList, XP_PopUpMenu, XP_Button,
  WinXP, Buttons, StdCtrls, Mask, raylightAutostart, inifiles;

type
  TfrmPrincipal = class(TForm)
    imgSkin: TImage;
    iclRadar: TImageList;
    iclNormal: TImageList;
    tmrHora: TTimer;
    tmrAjust: TTimer;
    tmrResto: TTimer;
    popMenu: TtfXPPopUpMenu;
    TrayIcon: TCoolTrayIcon;
    btnSair: TSpeedButton;
    btnMinimizar: TSpeedButton;
    btnFechar: TSpeedButton;
    btnSobre: TSpeedButton;
    btnLogoff: TSpeedButton;
    btnDesligar: TSpeedButton;
    btnReiniciar: TSpeedButton;
    lblHora: TLabel;
    mkeAjust: TMaskEdit;
    cbxLogoff: TCheckBox;
    cbxReiniciar: TCheckBox;
    cbxDesligar: TCheckBox;
    pmiAbrir: TMenuItem;
    N1: TMenuItem;
    pmiSair: TMenuItem;
    pmiStart: TMenuItem;
    N2: TMenuItem;
    AutoStart: TRaylightAutostart;
    pmiConfig: TMenuItem;
    pmiAutoConfig: TMenuItem;
    pmiACLogoff: TMenuItem;
    pmiACDesligar: TMenuItem;
    pmiACReiniciar: TMenuItem;
    procedure tmrHoraTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnLogoffClick(Sender: TObject);
    procedure btnMinimizarClick(Sender: TObject);
    procedure pmiAbrirClick(Sender: TObject);
    procedure btnDesligarClick(Sender: TObject);
    procedure btnReiniciarClick(Sender: TObject);
    procedure tmrRestoTimer(Sender: TObject);
    procedure tmrAjustTimer(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure pmiStartClick(Sender: TObject);
    procedure pmiACLogoffClick(Sender: TObject);
    procedure pmiACDesligarClick(Sender: TObject);
    procedure pmiACReiniciarClick(Sender: TObject);
  private
    { Private declarations }
  public
    strProgramado : String;
    strAtual, strProgram, strCont : String;
    SegundoAtual, SegundoProgram, SegundoTotal, quotient,
    hAtual, mAtual, sAtual, hProgram, mProgram, sProgram, hCont, mCont, sCont, hConta : Integer;
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

uses unitDlgCancela;

{$R *.dfm}

{   Sempre no topo  }
procedure ForceForegroundWindow(hwnd: THandle);
  // (W) 2001 Daniel Rolf
  // http://www.finecode.de
  // rolf@finecode.de
var
  hlp: TfrmPrincipal;
begin
  hlp := TfrmPrincipal.Create(nil);
  try
    hlp.BorderStyle := bsNone;
    hlp.SetBounds(0, 0, 1, 1);
    hlp.FormStyle := fsStayOnTop;
    hlp.Show;
    mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
    mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    SetForegroundWindow(hwnd);
  finally
    hlp.Free;
  end;
end;

{   Timer do Hor�rio  }
procedure TfrmPrincipal.tmrHoraTimer(Sender: TObject);
begin
  lblHora.Caption := TimeToStr(now);
end;

{  Form Create  }
procedure TfrmPrincipal.FormCreate(Sender: TObject);
Var
  ini:TiniFile;
begin
  //cria um mutex usando um nome �nico
  CreateMutex(nil, False, 'SoftCast.OnlyOne');

  //verifica se houve erro na cria��o
  if GetLastError = ERROR_ALREADY_EXISTS then begin
    MessageBox(0, 'Este programa j� est� sendo executado', 'Aviso', MB_ICONSTOP);
    Halt(0); // cancela execu��o
  end;

  TrayIcon.CycleIcons := False;
  TrayIcon.CycleInterval := 100;
  TrayIcon.IconList := iclNormal;
  TrayIcon.IconVisible := True;
  TrayIcon.PopupMenu := popMenu;
  {mkeAjust.EditMask := '!90:00:00;0;_';
  mkeAjust.Text := '';
  mkeAjust.AutoSelect := False;}
  pmiStart.Checked := AutoStart.autostart;
  If pmiStart.Checked = True Then Begin
    pmiAutoConfig.Enabled := True;
    Application.Minimize;
    TrayIcon.HideMainForm;
    ini:=TiniFile.Create('shutdownbytol.ini');
    If ini.ReadInteger('Config','AutoConfig',0) = 1 Then Begin
      If ini.ReadString('Config','funcao','ACLogoff') = 'ACLogoff' Then Begin
        mkeAjust.Text := ini.ReadString('Config','hora','000000');
        cbxLogoff.Checked := True;
        pmiACLogoff.Checked := True;
        pmiACDesligar.Enabled := False;
        pmiACReiniciar.Enabled := False;
        //btnLogoff.Enabled := False;
        btnDesligar.Enabled := False;
        btnReiniciar.Enabled := False;
        mkeAjust.Enabled := False;
        TrayIcon.IconList := iclRadar;
        TrayIcon.CycleIcons := True;
        //tmrResto.Enabled := True;
        strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
        TrayIcon.ShowBalloonHint('Logoff Autom�tico Ativado!', '�s '+ strProgramado +', seu computador efetuar� logoff automaticamente...', bitInfo, 10);
        tmrAjust.Enabled := True;
      End;
      If ini.ReadString('Config','funcao','ACDesligar') = 'ACDesligar' Then Begin
        mkeAjust.Text := ini.ReadString('Config','hora','000000');
        cbxDesligar.Checked := True;
        pmiACLogoff.Enabled := False;
        pmiACDesligar.Checked := True;
        pmiACReiniciar.Enabled := False;
        btnLogoff.Enabled := False;
        //btnDesligar.Enabled := False;
        btnReiniciar.Enabled := False;
        mkeAjust.Enabled := False;
        TrayIcon.IconList := iclRadar;
        TrayIcon.CycleIcons := True;
        //tmrResto.Enabled := True;
        strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
        TrayIcon.ShowBalloonHint('Desligamento Autom�tico Ativado!', '�s '+ strProgramado +', seu computador ir� desligar automaticamente...', bitInfo, 10);
        tmrAjust.Enabled := True;
      End;
      If ini.ReadString('Config','funcao','ACReiniciar') = 'ACReiniciar' Then Begin
        mkeAjust.Text := ini.ReadString('Config','hora','000000');
        cbxReiniciar.Checked := True;
        pmiACLogoff.Enabled := False;
        pmiACDesligar.Enabled := False;
        pmiACReiniciar.Checked := True;
        btnLogoff.Enabled := False;
        btnDesligar.Enabled := False;
        //btnReiniciar.Enabled := False;
        mkeAjust.Enabled := False;
          TrayIcon.IconList := iclRadar;
        TrayIcon.CycleIcons := True;
        //tmrResto.Enabled := True;
        strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
        TrayIcon.ShowBalloonHint('Reiniciamento Autom�tico Ativado!', '�s '+ strProgramado +', seu computador ir� reiniciar automaticamente...', bitInfo, 10);
        tmrAjust.Enabled := True;
      End;
    End;
  End;
end;

{   Bot�o Fechar  }
procedure TfrmPrincipal.btnFecharClick(Sender: TObject);
begin
  Halt;
end;

{   Bot�o Fun��o Logoff   }
procedure TfrmPrincipal.btnLogoffClick(Sender: TObject);
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
    If cbxLogoff.Checked = False Then Begin
      cbxLogoff.Checked := True;
      //btnLogoff.Enabled := False;
      btnDesligar.Enabled := False;
      btnReiniciar.Enabled := False;
      mkeAjust.Enabled := False;
      TrayIcon.IconList := iclRadar;
      TrayIcon.CycleIcons := True;
      //tmrResto.Enabled := True;
      strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
      TrayIcon.ShowBalloonHint('Logoff Autom�tico Ativado!', '�s '+ strProgramado +', seu computador efetuar� logoff automaticamente...', bitInfo, 10);
      tmrAjust.Enabled := True;
    End Else Begin
      cbxLogoff.Checked := False;
      pmiACLogoff.Checked := False;
      pmiACDesligar.Enabled := True;
      pmiACReiniciar.Enabled := True;
      //btnLogoff.Enabled := True;
      btnDesligar.Enabled := True;
      btnReiniciar.Enabled := True;
      mkeAjust.Enabled := True;
      TrayIcon.IconList := iclNormal;
      TrayIcon.CycleIcons := False;
      //tmrResto.Enabled := False;
      TrayIcon.ShowBalloonHint('Logoff Autom�tico Desativado!', 'Voc� acaba de cancelar o Logoff Autom�tico.', bitWarning, 10);
      tmrAjust.Enabled := False;
    End;
  End;
end;

{   Bot�o Minimizar   }
procedure TfrmPrincipal.btnMinimizarClick(Sender: TObject);
begin
  Application.Minimize;
  TrayIcon.HideMainForm;
end;

{   Bot�o-MenuPopUP Abrir   }
procedure TfrmPrincipal.pmiAbrirClick(Sender: TObject);
begin
  TrayIcon.ShowMainForm;
end;

{   Bot�o Desligar    }
procedure TfrmPrincipal.btnDesligarClick(Sender: TObject);
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
    If cbxDesligar.Checked = False Then Begin
      cbxDesligar.Checked := True;
      btnLogoff.Enabled := False;
      //btnDesligar.Enabled := False;
      btnReiniciar.Enabled := False;
      mkeAjust.Enabled := False;
      TrayIcon.IconList := iclRadar;
      TrayIcon.CycleIcons := True;
      //tmrResto.Enabled := True;
      strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
      TrayIcon.ShowBalloonHint('Desligamento Autom�tico Ativado!', '�s '+ strProgramado +', seu computador ir� desligar automaticamente...', bitInfo, 10);
      tmrAjust.Enabled := True;
    End Else Begin
      cbxDesligar.Checked := False;
      pmiACLogoff.Enabled := True;
      pmiACDesligar.Checked := False;
      pmiACReiniciar.Enabled := True;
      btnLogoff.Enabled := True;
      //btnDesligar.Enabled := True;
      btnReiniciar.Enabled := True;
      mkeAjust.Enabled := True;
      TrayIcon.IconList := iclNormal;
      TrayIcon.CycleIcons := False;
      //tmrResto.Enabled := False;
      TrayIcon.ShowBalloonHint('Desligamento Autom�tico Desativado!', 'Voc� acaba de cancelar o Desligamento Autom�tico.', bitWarning, 10);
      tmrAjust.Enabled := False;
    End;
  End;
end;

{   Bot�o Reiniciar   }
procedure TfrmPrincipal.btnReiniciarClick(Sender: TObject);
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
    If cbxReiniciar.Checked = False Then Begin
      cbxReiniciar.Checked := True;
      btnLogoff.Enabled := False;
      btnDesligar.Enabled := False;
      //btnReiniciar.Enabled := False;
      mkeAjust.Enabled := False;
      TrayIcon.IconList := iclRadar;
      TrayIcon.CycleIcons := True;
      //tmrResto.Enabled := True;
      strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
      TrayIcon.ShowBalloonHint('Reiniciamento Autom�tico Ativado!', '�s '+ strProgramado +', seu computador ir� reiniciar automaticamente...', bitInfo, 10);
      tmrAjust.Enabled := True;
    End Else Begin
      cbxReiniciar.Checked := False;
      pmiACLogoff.Enabled := True;
      pmiACDesligar.Enabled := True;
      pmiACReiniciar.Checked := False;
      btnLogoff.Enabled := True;
      btnDesligar.Enabled := True;
      //btnReiniciar.Enabled := True;
      mkeAjust.Enabled := True;
      TrayIcon.IconList := iclNormal;
      TrayIcon.CycleIcons := False;
      //tmrResto.Enabled := False;
      TrayIcon.ShowBalloonHint('Reiniciamento Autom�tico Desativado!', 'Voc� acaba de cancelar o Reiniciamento Autom�tico.', bitWarning, 10);
      tmrAjust.Enabled := False;
    End;
  End;
end;

{   Timer Tempo Restante    }
procedure TfrmPrincipal.tmrRestoTimer(Sender: TObject);
begin
  If tmrResto.Enabled = True Then Begin
    frmDlgCancela.Show;
    tmrResto.Enabled := False;
  End;
end;


{   Timer do Programa   }
procedure TfrmPrincipal.tmrAjustTimer(Sender: TObject);
begin
  strProgramado := Copy(mkeAjust.Text,1,2) + ':' + Copy(mkeAjust.Text,3,2) + ':' + Copy(mkeAjust.Text,5,2);
  {     LOGOFF    }
  If cbxLogoff.Checked = True Then Begin
    If CompareStr(strProgramado, TimeToStr(now)) = 0 Then Begin
      WinExec('logoff',0);
      tmrAjust.Enabled := False;
      Halt;
     End;
  End;
  {    DESLIGAR   }
  If cbxDesligar.Checked = True Then Begin
    If CompareStr(strProgramado, TimeToStr(now)) = 0 Then Begin
      Winexec('shutdown -s -f -t 20 -c "Desligamento solicitado pelo Shut Down by Telos Online"',0);
      tmrResto.Enabled := True;
      tmrAjust.Enabled := False;
    End;
  End;
  {   REINICIAR   }
  If cbxReiniciar.Checked = True Then Begin
    If CompareStr(strProgramado, TimeToStr(now)) = 0 Then Begin
      WinExec('shutdown -r -f -t 20 -c "Reiniciamento solicitado pelo Shut Down by Telos Online"',0);
      tmrResto.Enabled := True;
      tmrAjust.Enabled := False;
    End;
  End;
end;

procedure TfrmPrincipal.TrayIconDblClick(Sender: TObject);
begin
  TrayIcon.ShowMainForm;
end;

procedure TfrmPrincipal.pmiStartClick(Sender: TObject);
begin
  If pmiStart.Checked Then Begin
    If MessageDlg ('Deseja que o Shut Down, inicie automaticamente com o Windows?', mtConfirmation, [mbYes,mbNo],0) = mrYes Then Begin
      AutoStart.autostart := pmiStart.Checked;
      pmiAutoConfig.Enabled := True;
      TrayIcon.ShowBalloonHint('Iniciar junto com o Windows Ativado!', 'Sugerimos que voc� ative as configura��es autom�ticas, clicando com o bot�o direito do mouse no icone do Shut Down "Configura��es... > Ativar Configura��es Autom�ticas", caso queira que seu computador realiza a opera��o automaticamente.', bitInfo, 10);
    End;
  End Else
    AutoStart.autostart := pmiStart.Checked;
    pmiAutoConfig.Enabled := AutoStart.autostart;
    TrayIcon.ShowBalloonHint('', 'Voc� desabilitou a inicializa��o junto com o Windows.', bitInfo, 10);
end;

procedure TfrmPrincipal.pmiACLogoffClick(Sender: TObject);
Var
  ini:TiniFile;
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
  If pmiACLogoff.Checked Then Begin
    If MessageDlg ('Deseja ativar as Configura��es Autom�ticas?', mtConfirmation, [mbYes,mbNo],0) = mrYes Then Begin
      ini:=TiniFile.Create('shutdownbytol.ini');
      ini.WriteInteger('Config','AutoConfig',1);
      ini.WriteString('Config','funcao','ACLogoff');
      ini.WriteString('Config','hora',mkeAjust.Text);
      //pmiACLogoff.Enabled := False;
      pmiACDesligar.Enabled := False;
      pmiACReiniciar.Enabled := False;
      TrayIcon.ShowBalloonHint('Configura��es Autom�ticas Ativada!', 'A partir de agora, seu computador efetuar� Logoff automaticamente no hor�rio indicado.', bitInfo, 10);
    End;
  End Else Begin
    ini:=TiniFile.Create('shutdownbytol.ini');
    ini.WriteInteger('Config','AutoConfig',0);
    //pmiACLogoff.Enabled := True;
    pmiACDesligar.Enabled := True;
    pmiACReiniciar.Enabled := True;
    cbxLogoff.Checked := False;
    //btnLogoff.Enabled := True;
    btnDesligar.Enabled := True;
    btnReiniciar.Enabled := True;
    mkeAjust.Enabled := True;
    TrayIcon.IconList := iclNormal;
    TrayIcon.CycleIcons := False;
    //tmrResto.Enabled := False;
    tmrAjust.Enabled := False;
    TrayIcon.ShowBalloonHint('', 'As configura��es autom�ticas foram desativadas!', bitInfo, 10);
  End;
  End;
end;

procedure TfrmPrincipal.pmiACDesligarClick(Sender: TObject);
Var
  ini:TiniFile;
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
  If pmiACDesligar.Checked Then Begin
    If MessageDlg ('Deseja ativar as Configura��es Autom�ticas?', mtConfirmation, [mbYes,mbNo],0) = mrYes Then Begin
      ini:=TiniFile.Create('shutdownbytol.ini');
      ini.WriteInteger('Config','AutoConfig',1);
      ini.WriteString('Config','funcao','ACDesligar');
      ini.WriteString('Config','hora',mkeAjust.Text);
      pmiACLogoff.Enabled := False;
      //pmiACDesligar.Enabled := False;
      pmiACReiniciar.Enabled := False;
      TrayIcon.ShowBalloonHint('Configura��es Autom�ticas Ativada!', 'A partir de agora, seu computador ir� desligar automaticamente no hor�rio indicado.', bitInfo, 10);
    End;
  End Else Begin
    ini:=TiniFile.Create('shutdownbytol.ini');
    ini.WriteInteger('Config','AutoConfig',0);
    pmiACLogoff.Enabled := True;
    //pmiACDesligar.Enabled := True;
    pmiACReiniciar.Enabled := True;
    cbxDesligar.Checked := False;
    btnLogoff.Enabled := True;
    //btnDesligar.Enabled := True;
    btnReiniciar.Enabled := True;
    mkeAjust.Enabled := True;
    TrayIcon.IconList := iclNormal;
    TrayIcon.CycleIcons := False;
    //tmrResto.Enabled := False;
    tmrAjust.Enabled := False;
    TrayIcon.ShowBalloonHint('', 'As configura��es autom�ticas foram desativadas!', bitInfo, 10);
  End;
  End;
end;

procedure TfrmPrincipal.pmiACReiniciarClick(Sender: TObject);
Var
  ini:TiniFile;
begin
  If Copy(mkeAjust.Text,5,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O segundo especificado est� fora do padr�o.' + #13 + 'O segundo deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,3,2) > '60' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'O minuto especificado est� fora do padr�o.' + #13 +'O minuto deve ser menor que 60!', bitError, 10)
  Else If Copy(mkeAjust.Text,1,2) > '24' Then
    TrayIcon.ShowBalloonHint('Erro encontrado na Hora Programada!', 'A hora especificado est� fora do padr�o.' + #13 +'O hora deve ser menor que 24!', bitError, 10)
  Else Begin
  If pmiACReiniciar.Checked Then Begin
    If MessageDlg ('Deseja ativar as Configura��es Autom�ticas?', mtConfirmation, [mbYes,mbNo],0) = mrYes Then Begin
      ini:=TiniFile.Create('shutdownbytol.ini');
      ini.WriteInteger('Config','AutoConfig',1);
      ini.WriteString('Config','funcao','ACReiniciar');
      ini.WriteString('Config','hora',mkeAjust.Text);
      pmiACLogoff.Enabled := False;
      pmiACDesligar.Enabled := False;
      //pmiACReiniciar.Enabled := False;
      TrayIcon.ShowBalloonHint('Configura��es Autom�ticas Ativada!', 'A partir de agora, seu computador ir� reiniciar automaticamente no hor�rio indicado.', bitInfo, 10);
    End;
  End Else Begin
    ini:=TiniFile.Create('shutdownbytol.ini');
    ini.WriteInteger('Config','AutoConfig',0);
    pmiACLogoff.Enabled := True;
    pmiACDesligar.Enabled := True;
    //pmiACReiniciar.Enabled := True;
    cbxReiniciar.Checked := False;
    btnLogoff.Enabled := True;
    btnDesligar.Enabled := True;
    //btnReiniciar.Enabled := True;
    mkeAjust.Enabled := True;
    TrayIcon.IconList := iclNormal;
    TrayIcon.CycleIcons := False;
    //tmrResto.Enabled := False;
    tmrAjust.Enabled := False;
    TrayIcon.ShowBalloonHint('', 'As configura��es autom�ticas foram desativadas!', bitInfo, 10);
  End;
  End;
end;

end.
