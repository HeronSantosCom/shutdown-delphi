program shutdownbytol;

uses
  Forms,
  unitPrincipal in 'tmp_ico\unitPrincipal.pas' {frmPrincipal},
  unitDlgCancela in 'tmp_ico\unitDlgCancela.pas' {frmDlgCancela};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Shut Down by Telos Online';
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmDlgCancela, frmDlgCancela);
  Application.Run;
end.
